package com.stonerhawk.viewpager;

import java.util.List;
import java.util.Random;

import com.viewpagerindicator.TitlePageIndicator;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

public class ViewPagerSampleActivity extends Activity 
{
	private ViewPager viewPager = null;
	private TitlePageIndicator mTitleIndicator;
	private List<String> imageUrls;
	private Random randomGenerator = new Random();
	
	
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        viewPager = (ViewPager) findViewById(R.id.viewPager);
		MyPagerAdapter adapter = new MyPagerAdapter(getApplicationContext());
		viewPager.setAdapter(adapter);
		
		mTitleIndicator = (TitlePageIndicator) findViewById(R.id.indicator);
		mTitleIndicator.setViewPager(viewPager);
		mTitleIndicator.setCurrentItem(0);
    }
    
    public String anyUrl()
    {
        int index = randomGenerator.nextInt(imageUrls.size());
        String randomItem = imageUrls.get(index);
        return randomItem;
    }
	}